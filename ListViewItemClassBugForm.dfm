object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Delphi XE TListItemClass bug tester'
  ClientHeight = 450
  ClientWidth = 347
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lvTest: TListView
    Left = 0
    Top = 0
    Width = 347
    Height = 272
    Align = alClient
    Columns = <>
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsList
    OnCreateItemClass = lvTestCreateItemClass
    OnSelectItem = lvTestSelectItem
    ExplicitLeft = 128
    ExplicitTop = 200
    ExplicitWidth = 250
    ExplicitHeight = 150
  end
  object Memo1: TMemo
    Left = 0
    Top = 272
    Width = 347
    Height = 178
    Align = alBottom
    Lines.Strings = (
      'Please click the list items above, watch the list below fill.'
      ''
      
        'Then, with this process still running, connect to the machine us' +
        'ing '
      'RDP, and try again.')
    ReadOnly = True
    TabOrder = 1
  end
end
