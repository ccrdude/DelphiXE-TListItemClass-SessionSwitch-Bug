{
  This small form demonstrates a bug with custom TListItem classes in Delphi XE.

  Steps to reproduce:

  1. Run this program,
  2. click the list items to see what is displayed below,
  3. connect to this machine via RDP without closing the program,
  4. click again to see the TMemo is now empty.
}

unit ListViewItemClassBugForm;

interface

uses
   Windows,
   Messages,
   SysUtils,
   Variants,
   Classes,
   Graphics,
   Controls,
   Forms,
   Dialogs,
   ComCtrls,
   StdCtrls;

type
   TTestListItem = class(TListItem)
   private
      FTestLines: TStringList;
   public
      constructor Create(AOwner: TListItems); override;
      destructor Destroy; override;
      procedure Assign(Source: TPersistent); override;
      property TestLines: TStringList read FTestLines;
   end;

   TForm1 = class(TForm)
      lvTest: TListView;
      Memo1: TMemo;
      procedure lvTestCreateItemClass(Sender: TCustomListView; var ItemClass: TListItemClass);
      procedure FormCreate(Sender: TObject);
      procedure lvTestSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
   private
   public
   end;

var
   Form1: TForm1;

implementation

{$R *.dfm}

{ TTestListItem }

procedure TTestListItem.Assign(Source: TPersistent);
begin
   inherited;
   if Source is TTestListItem then begin
      Self.TestLines.Assign((Source as TTestListItem).TestLines);
      MessageDlg(Format('Source for %s is TTestListItem', [Self.Caption]), mtInformation, [mbOK], 0);
   end
   else begin
      MessageDlg(Format('Source for %s is NOT TTestListItem', [Self.Caption]), mtError, [mbOK], 0);
   end;
end;

constructor TTestListItem.Create(AOwner: TListItems);
begin
   inherited;
   FTestLines := TStringList.Create;
   MessageDlg(Format('TTestListItem.Create was called', [Self.Caption]), mtInformation, [mbOK], 0);
end;

destructor TTestListItem.Destroy;
begin
   FTestLines.Free;
   inherited;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
   li: TTestListItem;
begin
   li := lvTest.Items.Add as TTestListItem;
   li.Caption := 'Greeting';
   li.TestLines.Add('Hello World');
   li.TestLines.Add('Hallo Welt');
   li := lvTest.Items.Add as TTestListItem;
   li.Caption := 'Colour';
   li.TestLines.Add('Red');
   li.TestLines.Add('Green');
   li.TestLines.Add('Blue');
end;

procedure TForm1.lvTestCreateItemClass(Sender: TCustomListView; var ItemClass: TListItemClass);
begin
   ItemClass := TTestListItem;
end;

procedure TForm1.lvTestSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
   Memo1.Lines.Clear;
   if not Selected then begin
      Exit;
   end;
   if not Assigned(Item) then begin
      Exit;
   end;
   if not(Item is TTestListItem) then begin
      Exit;
   end;
   Memo1.Lines.Assign((Item as TTestListItem).TestLines);
end;

end.
